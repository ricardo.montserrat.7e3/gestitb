package cat.itb.gestitb;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListFragmentViewModel extends ViewModel
{
    private List<MissedAttendance> students;

    public ListFragmentViewModel()
    {
        students = new ArrayList<>();
        MissedAttendance student;
        for (int i = 0; i<100; i++)
        {
            student = new MissedAttendance();
            student.setStudentName("Student #"+(i+1));
            student.setJustified(i % 3 == 2);
            student.setDate(new Date());
            students.add(student);
        }
    }

    public List<MissedAttendance> getStudents() { return students; }

    public void setModules(String[] modules) { for (MissedAttendance student : students) student.setModule(modules[(int)Math.floor(Math.random() * modules.length)]); }
}
