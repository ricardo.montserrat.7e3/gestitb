package cat.itb.gestitb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment missedAttendanceItem = getSupportFragmentManager().findFragmentById(R.id.fragment_container);

        if(missedAttendanceItem == null)
        {
            missedAttendanceItem = new MissedAttendanceListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, missedAttendanceItem).commit();
        }
    }
}