package cat.itb.gestitb;

import java.util.Date;

public class MissedAttendance
{
    private String studentName, module;
    private Date date;
    private boolean justified;

    public MissedAttendance()
    {
        studentName = "";
        module = "";
        date = new Date();
        justified = false;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) { this.module = module; }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isJustified() {
        return justified;
    }

    public void setJustified(boolean justified) {
        this.justified = justified;
    }
}