package cat.itb.gestitb;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MissedAttendanceAdapter extends RecyclerView.Adapter<MissedAttendanceAdapter.MissedAttendanceViewHolder>
{
    List<MissedAttendance> missedAttendances;

    public MissedAttendanceAdapter(List<MissedAttendance> missedAttendances) { this.missedAttendances = missedAttendances; }

    public class MissedAttendanceViewHolder extends RecyclerView.ViewHolder
    {
        TextView studentName, module, date;
        ImageView justified;

        public MissedAttendanceViewHolder(@NonNull View itemView) {
            super(itemView);
            studentName = itemView.findViewById(R.id.studentName);
            module = itemView.findViewById(R.id.textViewModules);
            date = itemView.findViewById(R.id.textViewDate);
            justified = itemView.findViewById(R.id.imageViewJustified);
        }

        public void bindData(MissedAttendance missedAttendance) { if(missedAttendance != null)
        {
            studentName.setText(missedAttendance.getStudentName());
            module.setText(missedAttendance.getModule());
            date.setText(missedAttendance.getDate().toString());
            justified.setImageResource(missedAttendance.isJustified()? R.drawable.ic_justified : R.drawable.ic_not_justified);
        }
        }
    }

    @NonNull
    @Override
    public MissedAttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.missed_attendance_list_item_fragment, parent, false);
        return new MissedAttendanceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MissedAttendanceViewHolder holder, int position) { holder.bindData(missedAttendances.get(position)); }

    @Override
    public int getItemCount() { return missedAttendances.size(); }
}
