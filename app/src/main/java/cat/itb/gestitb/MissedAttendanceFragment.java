package cat.itb.gestitb;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Date;

public class MissedAttendanceFragment extends Fragment
{
    private String[] modules;
    private MissedAttendance studentMissed;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        studentMissed = new MissedAttendance();
        modules = getResources().getStringArray(R.array.modules);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.missed_attendance_fragment, container, false);

        v.findViewById(R.id.editTextTextPersonName).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) { studentMissed.setStudentName(((EditText)v).getText().toString()); }
            }
        });

        //Save Spinner Info on Click Item

        ((Spinner) v.findViewById(R.id.modules)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                studentMissed.setModule(modules[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        //Show, save and disable button
        Button dateButton = v.findViewById(R.id.buttonDate);
        Date date = new Date();

        dateButton.setText(date.toString());
        dateButton.setEnabled(false);
        studentMissed.setDate(date);

        //Save Checkbox value
        v.findViewById(R.id.checkBoxJustified).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                studentMissed.setJustified(((CheckBox)v.findViewById(R.id.checkBoxJustified)).isChecked());
            }
        });

        final EditText name = v.findViewById(R.id.editTextTextPersonName);

        //Show Values and reset everything
        v.findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                studentMissed.setStudentName(name.getText().toString());
                String message = "The student " + studentMissed.getStudentName() + " has missed " + studentMissed.getModule() + " on " + studentMissed.getDate().toString() + " with " + (studentMissed.isJustified()?"justification.":"no justification!");

                new AlertDialog.Builder(getContext()).setTitle("Missed attendance created").setMessage(message).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
        return v;
    }
}
